# Training Material for Research Data Management<bc>
# at [Hub Matter-Helmholtz Metadata Collaboration (HMC)](https://helmholtz-metadaten.de/en/matter/overview)
## and Helmholtz Zentrum Berlin (HZB)

Collection of training material and tools used for the development of a Training offer in RDM

The framework is the Training and Outreach activities of Hub Matter-HMC at HZB

This project is used for material distribution and review.
Currently the material consist of:
* [slides](https://gitlab.helmholtz-berlin.de/a2395/training_material1/-/blob/master/intermediate/slides)
* [jupyter notebooks](https://gitlab.helmholtz-berlin.de/a2395/training_material1/-/blob/master/intermediate/notebooks/)
    - A [WIKI](https://gitlab.hzdr.de/konstantin.walter/training_material1-TUTORIAL/-/wikis/home) explaining how to use these Notebooks
* [design templates](https://gitlab.helmholtz-berlin.de/a2395/training_material1/-/edit/master/trainer)

Material is organized in expertise levels: Beginners, Intermediate, Experts/Trainers

The Jupyter notebooks are compatible with Jupyterlab3, see requirements.txt /environment.yml for details.

End of Sept'22  the installation instructions based on  `conda`, `venv`, `poetry` will be ready.


!! the repository is in progress, branch 39 is final for intermediate level training, 
with guidelines for creating a python kernel

please start an issue for questions and clarifications !!

The notebook can be run in Jupyterlab (v3 is tested) and Python3.8 is compatible with the packages used in the ipynb files 
hosted in this repository. 

